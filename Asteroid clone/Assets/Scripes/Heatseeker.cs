﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heatseeker : MonoBehaviour {

	public Transform targetTf;
	private Transform tf;
	public float speed;
	public bool isAlwaysSeeking;
	private Vector3 movementVector;
	public bool isDirectional;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		movementVector = targetTf.position - tf.position;

	}
	
	// Update is called once per frame
	void Update () {
		if (isAlwaysSeeking) {
			movementVector = targetTf.position - tf.position;
		}
			movementVector.Normalize ();
			movementVector = movementVector * speed;
			tf.position = tf.position = movementVector;
		if (isDirectional) {
			tf.right = -movementVector;
		}
	}
}
