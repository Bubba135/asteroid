﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public Transform tf;
	public float speed;
	public List<GameObject> ignoreMe;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	public void OnTriggerEnter2D (Collider2D otherCollider){
		for (int i = 0; i < ignoreMe == ignoreMe[i]){
			if (otherCollider.gameObject == ignoreMe[i]){
				return;
			}
		}


		Destroy (otherCollider.gameObject);
		Destroy (gameObject);
	}

	public void Move (){
		tf.position += tf.right * speed; 

	}
}
