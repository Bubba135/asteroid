﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Changecolor : MonoBehaviour {

	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D () {
		sr.color = Color.red;
}
	void OnCollisionExit2D () {
		sr.color = Color.green;
}
	void OnCollisionStay () {
		Debug.Log ("Ouch");
}
}